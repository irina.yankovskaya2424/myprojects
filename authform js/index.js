
let email = document.forms['form']['email'];
let password = document.forms['form']['password'];
let passwordRepeat = document.forms['form']['passwordRepeat'];

function validateUserLogin() {
    if (email.value.length < 9) {
        alert('Email must contain more then 8 symbols');
        return false;
    }

    if (password.value.length < 6) {
        alert('Wrong password');
        return false;
    }

    if (sessionStorage.getItem(email.value) !== password.value) {
        let errMsg = 'User does not exists';
        alert(errMsg);
        return false;
    }
}

function createNewUser() {
    if (email.value.length < 9) {
        alert('Email must contain more then 8 symbols');
        return false;
    }

    if (password.value.length < 6) {
        alert('Password must contain more then 5 symbols');
        return false;
    }

    if (passwordRepeat.value !== password.value) {
        alert('Passwords should be the same');
        return false;
    }

    sessionStorage.setItem(email.value, password.value);

    return true;
}