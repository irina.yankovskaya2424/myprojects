let startMinRange = 0;
let startMaxRange = 8;
let startPrizeMoney = 100;

let prizeMoneyNextLevels = startPrizeMoney;
let maxRangeNextLevels = startMaxRange;
let userNumber;
let totalPrice = 0;

let ask = confirm('Do you want to play a game?');

if (ask === true) {
    startGame(startPrizeMoney, startMinRange, startMaxRange);
} else {
    alert('You did not become a billionaire, but can.');
}

function startGame(prizeMoney, minRange, maxRange) {
    let gameResult = play(prizeMoney, minRange, maxRange);

    if (!gameResult) {
        alert('Thank you for your participation. Your prize is: ' + totalPrice + '$');
        let ask = confirm('Do you want to play again?');
        setDefaultData();

        while (ask === true) {
            gameResult = play(startPrizeMoney, startMinRange, startMaxRange);
            if (gameResult) {
                playNextLevel();
                break;
            }
            alert('Thank you for your participation. Your prize is: ' + totalPrice + '$');
            ask = confirm('Do you want to play again222?');
        }
    } else {
        playNextLevel();
    }
}

function play(prizeMoney, minRange, maxRange) {
    let attempt = 0;
    let range = getRandomInRange(minRange, maxRange);

    // alert(range + ' ' + prizeMoney + ' ' + minRange + ' ' + maxRange);

    for (let i = 3; i > attempt; i--) {
        let dividePrize = 2;
        userNumber = parseInt(prompt('Choose a roulette number from ' + minRange + ' to ' + maxRange + '\n' +
            'Attempts left: ' + i + '\n' +
            'Total prize: ' + totalPrice + ' $ \n' +
            'Possible prize on current attempt: ' + prizeMoney + ' $', '0'));
        if (userNumber === range) {
            totalPrice += prizeMoney;
            return true;
        }

        prizeMoney /= dividePrize;
    }
}

function playNextLevel() {
    let askStartNextLevel = confirm('Congratulation, you won! Your prize is: ' +
        totalPrice + '$. Do you want to continue?');

    if (askStartNextLevel) {
        let multiplyPrize = 2;
        let plusMaxRange = 4;
        prizeMoneyNextLevels *= multiplyPrize;
        maxRangeNextLevels += plusMaxRange;
        startGame(prizeMoneyNextLevels, startMinRange, maxRangeNextLevels);
    } else {
        alert('Thank you for your participation. Your prize is: ' + totalPrice + '$');
        ask = confirm('Do you want to play again333?');

        if (ask === true) {
            setDefaultData();
            startGame(startPrizeMoney, startMinRange, startMaxRange);
        }
    }
}

function setDefaultData() {
    totalPrice = 0;
    prizeMoneyNextLevels = startPrizeMoney;
    maxRangeNextLevels = startMaxRange;
}

function getRandomInRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
