let initialDepositAmount = parseInt(prompt('Enter the initial amount of money', '1000'));
let depositUserYears = parseInt(prompt('Enter the number of years', '10'));
let percentageOfYear = parseInt(prompt('Enter the percentage', '10'));
let totalAmount = initialDepositAmount;

const minInitialDepositAmount = 1000;
const minDepositUserYears = 1;
const minPercentageOfYear = 100;
const oneHundredPercents = 100;

while (validateIntValue(initialDepositAmount) || validateIntValue(depositUserYears) ||
validateIntValue(percentageOfYear)) {
    alert('Input Int values only');
    userData();
}

while (initialDepositAmount < minInitialDepositAmount || depositUserYears < minDepositUserYears ||
percentageOfYear > minPercentageOfYear) {
    alert('Invalid input data');
    userData();
}

for (let i = 0; i < depositUserYears; i++) {
    totalAmount = calculatePercentage(totalAmount, percentageOfYear);
}

let totalProfit = totalAmount - initialDepositAmount;

alert('Initial amount: ' + initialDepositAmount + '\n' + 'Number of years: ' + depositUserYears + '\n' +
    'Percentage of ' + 'year: ' + percentageOfYear + '\n\n' + 'Total profit ' + Math.round(totalProfit) + '\n' +
    'Total amount: ' + Math.round(totalAmount));

function calculatePercentage(amount, percentage) {
    return amount + amount / oneHundredPercents * percentage;
}

function userData() {
    initialDepositAmount = parseInt(prompt('Enter the initial amount of money', '1000'));
    depositUserYears = parseInt(prompt('Enter the number of years', '10'));
    percentageOfYear = parseInt(prompt('Enter the percentage', '10'));
    totalAmount = initialDepositAmount;
}

function validateIntValue(value) {
    return Number.isNaN(value) === true;
}
