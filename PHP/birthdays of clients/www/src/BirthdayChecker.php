<?php

class  BirthdayChecker
{
    public function checkClientBirthday($user)
    {
        $currentDate = date("Y-m-d");

        //TODO replace $user->getBirthdayDate() to $user['bdate'] for prod
        if($currentDate == $user->getBirthdayDate()){
            return($user);
        }
    }
}
