<?php

class User
{
    private $firstName;
    private $lastName;
    private $birthdayDate;
    private $email;
    private $mobileNumber;

    public function __construct($firstName, $lastName, $birthdayDate, $email, $mobileNumber)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->birthdayDate = $birthdayDate;
        $this->email = $email;
        $this->mobileNumber = $mobileNumber;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    public function getBirthdayDate()
    {
        return $this->birthdayDate;
    }

    public function setBirthdayDate($birthdayDate)
    {
        $this->birthdayDate = $birthdayDate;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getMobileNumber()
    {
        return $this->mobileNumber;
    }

    public function setMobileNumber($mobileNumber)
    {
        $this->mobileNumber = $mobileNumber;
    }

    public function __toString()
    {
        return 'Клиент' . ' ' . $this->getFirstName() . ' ' . $this->getLastName()
            . ' сегодня празднует день рождения. ' . '<br/>'
            . ' Вы можете поздравить его по номеру телефона ' . $this->getMobileNumber()
            . ' или написать ему на почту ' . $this->getEmail();
    }
}

