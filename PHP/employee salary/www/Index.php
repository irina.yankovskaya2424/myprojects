<?php
include 'EmployeeSalary.php';

/* ----- CASE 1 ----
У сотрудника нет никаких записей о ставке
*/
//$salaryList = array();

/* ----- CASE 2 ----
У сотрудника есть запись о ставке только за прошлый месяц|позапрошлый месяц и т.д.
*/
//$salary1 = new EmployeeSalary(3000, '05-09-2021');
//$salaryList = array($salary1);


/* ----- CASE 3 ----
У сотрудника есть запись о ставке за текущий месяц (одна или более записей).
Нет записей о ставке за предыдущие месяцы.
*/
//$salary1 = new EmployeeSalary(3000, '05-11-2021');
//$salary2 = new EmployeeSalary(4231, '08-11-2021');
//$salary3 = new EmployeeSalary(5743, '17-11-2021');
//$salaryList = array($salary3, $salary1, $salary2);
// $salaryRateByPreviousMonth = null;

/* ----- CASE 4 ----
У сотрудника есть запись о ставке за текущий месяц (одна или более записей).
Также есть запись о ставке с предыдущего месяца
месяц   ЗП    ЗП_ДЕНЬ   ЗП_С_ПО
1-4 =   900   30        120
5-7 =   3000  100       300
8-16 =  4231  141.03    1269.27
17-22 = 5743  191.43    1148.58
Итого: 2837.85
*/
$salary1 = new EmployeeSalary(3000, '05-11-2021');
$salary2 = new EmployeeSalary(4231, '08-11-2021');
$salary3 = new EmployeeSalary(5743, '17-11-2021');
$salary4 = new EmployeeSalary(900, '05-09-2021');
$salaryList = array($salary3, $salary1, $salary2);
$salaryRateByPreviousMonth = $salary4;

function calculateAccruedSalaryPerMonth($salaryList)
{
    // сортируем даты ставок от меньшего к большему
    usort($salaryList, 'compareDates');
    $accruedSalaryPerCurrentDate = 0.00;
    $currentDate = date('d-m-Y');
    $numberOfDaysInCurrentMonth = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));

    // если ставок по сотруднику нет, возвращаем смс
    if ($salaryList == null || count($salaryList) == 0) {
        echo 'Ставка не найдена';
    // если в базе нет ставок по текущему месяцу, но есть ближайшая из предыдущего месяца, считаем по ней
    } else if (count($salaryList) == 1 && ((getMonthFromDate($currentDate) > getMonthFromDate($salaryList[0]->getStartDate())))) {
        $accruedSalaryPerCurrentDate += calculateSalaryByPeriod(getFirstDayFromCurrentMonth(), $currentDate, getDailySalary($salaryList[0]->getSalary(), $numberOfDaysInCurrentMonth));
    } else {
    // если есть изменение ставки (или ставок) по текущему месяцу, просчитываем по каждому периоду
        for ($i = 0; $i < count($salaryList) - 1; $i++) {
            $start = $salaryList[$i]->getStartDate();
            $end = $salaryList[$i + 1]->getStartDate();
            $dailySalaryByPeriod = getDailySalary($salaryList[$i]->getSalary(), $numberOfDaysInCurrentMonth);
            $accruedSalaryPerCurrentDate += calculateSalaryByPeriod($start, $end, $dailySalaryByPeriod);
        }

        // просчитываем остаток от последнего изменения ставки до вчерашней даты
        // (считаем до вчерашней даты включительно т.к. сегодня еще не закончилось)
        $lastActualSalary = $salaryList[count($salaryList) - 1];

        if (getDayFromDate($currentDate) > getDayFromDate($lastActualSalary->getStartDate())) {
            $accruedSalaryPerCurrentDate +=
                calculateSalaryByPeriod($lastActualSalary->getStartDate(), $currentDate,
                    getDailySalary($lastActualSalary->getSalary(), $numberOfDaysInCurrentMonth));
        }

        // проверяем, если изменение даты ставки было после 1-го числа тек.месяца,
        // пробуем просчитать ставку с начала месяца до первого изменения в текущем месяце (например, с 1 по 5 число)
        if (getDayFromDate($salaryList[0]->getStartDate()) > getDayFromDate(getFirstDayFromCurrentMonth())) {
            // если мы не нашли предыдущих ставок, это означает что сотрудник начал работать с 5-го числа и не
            // нужно просчитывать ставку с 1-го по 5-е число. Если предыдущая ставка есть, просчитваем недостающий период
            $rateByPreviousPeriod = $GLOBALS['salaryRateByPreviousMonth'];
            if ($rateByPreviousPeriod != null) {
                $accruedSalaryPerCurrentDate +=
                    calculateSalaryByPeriod(getFirstDayFromCurrentMonth(),
                        $rateByPreviousPeriod->getStartDate(),
                        getDailySalary($rateByPreviousPeriod->getSalary(), $numberOfDaysInCurrentMonth));
            }
        }
    }

    return $accruedSalaryPerCurrentDate;
}


function calculateSalaryByPeriod($start, $end, $dailySalary)
{
    $startDate = getDayFromDate($start);
    $endDate = getDayFromDate($end);

    return ($endDate - $startDate) * $dailySalary;
}

function getDailySalary($salary, $numberOfDays)
{
    return number_format((float)($salary / $numberOfDays), 2, '.', '');
}

function getDayFromDate($date)
{
    return DateTime::createFromFormat("d-m-Y", $date)->format("d");
}

function getMonthFromDate($date)
{
    return DateTime::createFromFormat("d-m-Y", $date)->format("m");
}

function getFirstDayFromCurrentMonth()
{
    return date('01-m-Y');
}

function compareDates($date1, $date2)
{
    $d1 = strtotime($date1->getStartDate());
    $d2 = strtotime($date2->getStartDate());
    return $d1 - $d2;
}

echo calculateAccruedSalaryPerMonth($salaryList);

