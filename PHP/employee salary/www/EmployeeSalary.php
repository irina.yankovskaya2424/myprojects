<?php


class EmployeeSalary
{
private $salary;
private $startDate;


    public function __construct($salary, $startDate)
    {
        $this->salary = $salary;
        $this->startDate = $startDate;
    }

    public function getSalary()
    {
        return $this->salary;
    }


    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }
}