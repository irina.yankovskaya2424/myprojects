<?php

$startTime = strtotime('9:00');
$finishTime = strtotime('9:43');

function calculateEmployeeSpendTimeInMinutes($startTime, $finishTime)
{
    return ($finishTime - $startTime) / 60;
}

$employeeSpendTimesInMinutes = [23, 45, 87];

function calculateAverageSpendTime($times)
{
    $commonTimeInMinutes = 0;

    foreach ($times as $time) {
        $commonTimeInMinutes += $time;
    }

    $hours = 0;
    $minutes = $commonTimeInMinutes / count($times);;

    while ($minutes > 60) {
        $hours++;
        $minutes -= 60;
    }
    echo 'Среднее время работы мастера ' . $hours .':'.floor($minutes);
}

calculateAverageSpendTime($employeeSpendTimesInMinutes);