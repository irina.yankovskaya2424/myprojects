<?php

class Master
{
    private $firstName;
    private $lastName;
    private $blockedTimes;

    public function __construct($firstName, $lastName, $blockedTimes)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->blockedTimes = $blockedTimes;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    public function getBlockedTimes()
    {
        return $this->blockedTimes;
    }

    public function setBlockedTimes($blockedTimes)
    {
        $this->blockedTimes = $blockedTimes;
    }
}