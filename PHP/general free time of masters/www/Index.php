<?php
include 'src\Master.php';

// TEST DATA
$ivan = new Master("Иван", "Иванов", ["10:00-11:00", "11:00-12:00", "17:00-18:00"]);
$petr = new Master("Петр", "Петров", ["11:00-12:00", "16:00-17:00"]);
$mastersList = array($ivan, $petr);
$schedule = ["09:00-10:00", "10:00-11:00", "11:00-12:00", "12:00-13:00", "13:00-14:00", "14:00-15:00", "15:00-16:00", "16:00-17:00", "17:00-18:00"];

function getCommonAvailableTime($mastersList, $schedule)
{
    $result = $schedule;

    for ($i = 0; $i < count($mastersList); $i++) {
        for ($j = 0; $j < count($mastersList[$i]->getBlockedTimes()); $j++) {
            $indexToDelete = array_search($mastersList[$i]->getBlockedTimes()[$j], $result);
            if ($indexToDelete) {
                unset($result[$indexToDelete]);
            }
        }
        $result = array_values($result);
    }

    return $result;
}

printAvailableTime(getCommonAvailableTime($mastersList, $schedule));

function printAvailableTime($availableTime)
{
    echo "Общее свободное время всех мастеров: <br>";
    foreach ($availableTime as $time) {
        echo "$time<br>";
    }
}


