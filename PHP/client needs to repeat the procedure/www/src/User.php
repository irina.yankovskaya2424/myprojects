<?php

class User
{
    private $firstName;
    private $lastName;
    private $email;
    private $mobileNumber;
    private $procedures;

    public function __construct($firstName, $lastName, $email, $mobileNumber, $procedures)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->mobileNumber = $mobileNumber;
        $this->procedures = $procedures;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getMobileNumber()
    {
        return $this->mobileNumber;
    }
    public function setMobileNumber($mobileNumber)
    {
        $this->mobileNumber = $mobileNumber;
    }
    public function getProcedures()
    {
        return $this->procedures;
    }
    public function setProcedures($procedures)
    {
        $this->procedures = $procedures;
    }
}

