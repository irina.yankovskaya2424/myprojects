<?php

class Procedure
{
private $name;
private $numberOfProcedures;
private $numberOfCompleteProcedures;
private $lastProcedureDate;

    public function __construct($name, $numberOfProcedures, $numberOfCompleteProcedures, $lastProcedureDate)
    {
        $this->name = $name;
        $this->numberOfProcedures = $numberOfProcedures;
        $this->numberOfCompleteProcedures = $numberOfCompleteProcedures;
        $this->lastProcedureDate = $lastProcedureDate;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getNumberOfProcedures()
    {
        return $this->numberOfProcedures;
    }

    public function setNumberOfProcedures($numberOfProcedures)
    {
        $this->numberOfProcedures = $numberOfProcedures;
    }

    public function getNumberOfCompleteProcedures()
    {
        return $this->numberOfCompleteProcedures;
    }

    public function setNumberOfCompleteProcedures($numberOfCompleteProcedures)
    {
        $this->numberOfCompleteProcedures = $numberOfCompleteProcedures;
    }

    public function getLastProcedureDate()
    {
        return $this->lastProcedureDate;
    }

    public function setLastProcedureDate($lastProcedureDate)
    {
        $this->lastProcedureDate = $lastProcedureDate;
    }
}