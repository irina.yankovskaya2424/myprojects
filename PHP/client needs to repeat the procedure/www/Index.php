<?php

include 'src\User.php';
include 'src\Procedure.php';


function initTestData()
{
    $procedure1 = new Procedure('Лазерная эпиляция', 10, 8, '14-10-2021');
    $procedure2 = new Procedure('Пилинг', 4, 2, '11-05-2021');
    $procedure3 = new Procedure('Чистка лица', 5, 4, '18-10-2021');

    $natashaProcedures = [$procedure1, $procedure2, $procedure3];
    $natasha = new User('Наташа', 'Пампампам', 'natasha@gmail.com', '380665435622', $natashaProcedures);

    $procedure4 = new Procedure('Перманент бровей', 2, 1, '14-10-2021');

    $alinaProcedures = [$procedure4];
    $alina = new User('Алина', 'Тамтамтам', 'alina@gmail.com', '380665435499', $alinaProcedures);

    return array($natasha, $alina);
}


$clients = initTestData();

function checkClientsNotifications($clients)
{
    for ($i = 0; $i < count($clients); $i++) {
        for ($j = 0; $j < count($clients[$i]->getProcedures()); $j++) {
            showNotificationIfRequired($clients[$i], $clients[$i]->getProcedures()[$j]);
        }
    }

}

function showNotificationIfRequired($client, $procedure)
{
    $currentDate = time();
    $lastVisitedDate = strtotime($procedure->getLastProcedureDate());
    $delta = round(($currentDate - $lastVisitedDate) / (60 * 60 * 24));


    if (($procedure->getNumberOfProcedures() >= $procedure->getNumberOfCompleteProcedures()) && $delta == 30) {
        echo 'У клиента ' . $client->getFirstName() . ' ' . $client->getLastName() . ' прошло ' . $delta .
            ' дней с момента последней процедуры  "' . $procedure->getName() . '"<br>';
        echo 'Сделано ' . $procedure->getNumberOfCompleteProcedures() . ' из ' . $procedure->getNumberOfProcedures() . '<br>';
        echo 'Тел: '.$client->getMobileNumber().'<br><br>';
    }
}

checkClientsNotifications($clients);
