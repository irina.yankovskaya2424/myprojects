<?php

include 'dbConfig.php';

$querySelectRemains = "SELECT  * FROM remains";
$queryCategories = "SELECT DISTINCT(category) FROM remains";


$remains = $con->query($querySelectRemains);
$categories = $con->query($queryCategories);


?>

<html>
<head>
    <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">
    <style>
        th, td {
            border: 1px solid;
            padding: 5px 10px;
        }

        td{
            background: white;
        }

        th {
            background: #6900c7;
            color: white;
            font-size: small;
            cursor: pointer;
        }
    </style>
</head>
<body style="background: azure">
<div style="display: flex; justify-content: center; align-items: baseline">
    <a href="#" style="background: white; border: 1px solid #69707a; height: max-content; padding: 7px; margin-right: 10px" onclick="print(); return false"><i class="fa fa-print" style="color: #69707a;; font-size: small"></i></a>
    <select style="padding: 5px 10px; margin-bottom: 20px; margin-top: 10px; background: #6900c7; color: white;"
            id="categoriesDropdown" oninput="filterTable()">
        <option>Все категории</option>
        <?php
        while ($row = $categories->fetch_assoc()) { ?>
            <option><?php echo $row["category"] ?></option>
        <?php } ?>
    </select>
</div>


<table id="myTable" class="sort" style="margin: 0 auto">
    <thead align="left" style="display: table-header-group">
    <tr>
        <th>id</th>
        <th>Артикул</th>
        <th>Штрих-код</th>
        <th>Название</th>
        <th>Тип</th>
        <th>Категория</th>
        <th>Остаток</th>
        <th>Цена</th>
        <th>Сумма</th>
    </tr>
    </thead>
    <tbody>

    <?php
    while ($row = $remains->fetch_assoc()) { ?>
        <tr class="item_row">
            <td><?php echo $row["id"] ?></td>
            <td><?php echo $row["article"] ?></td>
            <td><?php echo $row["code"] ?></td>
            <td><?php echo $row["name"] ?></td>
            <td><?php echo $row["type"] ?></td>
            <td><?php echo $row["category"] ?></td>
            <td><?php echo $row["remain"] ?></td>
            <td><?php echo $row["price"] . ' ₴' ?></td>
            <td><?php echo $row["amount"] . ' ₴' ?></td>
        </tr>
    <?php } ?>

    </tbody>
</table>

<script>
    window.addEventListener("DOMContentLoaded", function() {
        (function(f) {
            function g(c) {
                return function(b, a) {
                    b = b.cells[c].textContent;
                    a = a.cells[c].textContent;
                    b = +b || b;
                    a = +a || a;
                    return b > a ? 1 : b < a ? -1 : 0
                }
            }
            let d = document.querySelector(f),
                e = [].slice.call(d.rows, 1);
            [].slice.call(d.rows[0].cells).forEach(function(c, b) {
                let a = 0;
                c.addEventListener("click", function() {
                    e.sort(g(b));
                    a && e.reverse();
                    e.forEach(function(a) {
                        d.appendChild(a)
                    });
                    a ^= 1
                })
            })
        })(".sort")
    });



    function filterTable() {
        let dropdown, table, rows, cells, category, filter;
        dropdown = document.getElementById("categoriesDropdown");
        table = document.getElementById("myTable");
        rows = table.getElementsByTagName("tr");
        filter = dropdown.value;

        for (let row of rows) {
            cells = row.getElementsByTagName("td");
            category = cells[5] || null;
            if (filter === "Все категории" || !category || (filter === category.textContent)) {
                row.style.display = "";
            } else {
                row.style.display = "none";
            }
        }
    }
</script>
</body>
</html>
