drop database if exists testdb;
create database testdb;
use testdb;


CREATE TABLE `remains`
(
    `id`       int NOT NULL AUTO_INCREMENT,
    `article`  varchar(30),
    `code`     int,
    `name`     varchar(30),
    `type`     varchar(30),
    `category` varchar(30),
    `remain`   double,
    `price`    double,
    `amount`   double,
    PRIMARY KEY (`id`)
);

insert into remains VALUES
(1, 'MT1',null, 'test1', 'Материалы', 'Джойка окрашивание', '1.00', '11.00', '111.00'),
(2, 'MT2',null, 'test2', 'Материалы', 'Бьюти товары', '2.00', '22.00', '222.00'),
(3, 'MT3',null, 'test3', 'Материалы', 'Бьюти товары', '3.00', '33.00', '333.00'),
(4, 'MT4',null, 'test4', 'Материалы', 'Расходники', '4.00', '44.00', '444.00'),
(5, 'MT5',null, 'test5', 'Материалы', 'Бьюти товары', '5.00', '55.00', '555.00'),
(6, 'MT1',null, 'test1', 'Материалы', 'Джойка окрашивание', '1.00', '11.00', '111.00'),
(7, 'MT2',null, 'test2', 'Материалы', 'Расходники', '2.00', '22.00', '222.00'),
(8, 'MT3',null, 'test3', 'Материалы', 'Джойка окрашивание', '3.00', '33.00', '333.00'),
(9, 'MT4',null, 'test4', 'Материалы', 'Джойка окрашивание', '4.00', '44.00', '444.00'),
(10, 'MT5',null, 'test5', 'Материалы', 'Джойка окрашивание', '5.00', '55.00', '555.00');